# Pur Beurre - Du gras, oui mais de qualité

Pur Beurre is an application made for Project 10 of OpenClassrooms DA Python.

It is a Django application that allows you to find a food product and choose a substitute with a better nutrition grade.

## Getting Started

You can use is directly at this adress: [http://157.230.114.23/](https://http://157.230.114.23/)

### Prerequisites

This application works with Python 3.8 and Django 3.0.7

You can choose the SGDB you want, just configure in [*settings.py*](https://github.com/Anubis-LT/PROJET10/blob/master/purbeurre/settings.py)

### Installing

clone this repo

    git clone https://github.com/Anubis-LT/PROJET10.git

create the virtualenv and install dependencies

    cd Projet10
    python -m venv venv

create environment variables (here is for development config)

    ENV=DEV 
    SECRET_KEY= [SECRETKEY] 
    

I used Postgresql but for local use, you may choose whatever database language. You'll have to create a database named p8-purbeurre, a user and a password. you'll setup it in settings.py

If you use a different SGDB you can configure it in [*settings.py*](https://github.com/Anubis-LT/PROJET10/blob/master/purbeurre/settings.py)

Run migrate and fill in database (here with 50 product of each category)

    purbeurre/manage.py migrate
    purbeurre/manage.py fillindb 50

Then run server

    purbeurre/manage.py runserver

## Running the tests

    test purbeurre/

## Deployment

Deployement config is set for Heroku. You need to create environment variables in Heroku

    ENV=PRODUCTION
    SECRET_KEY= [SECRETKEY]

The production configuration is set for Postgres, activate it in Heroku.

    heroku addons:create heroku-postgresql:hobby-dev

Keep in mind to migrate and fill in database.

## Configuration

You can modify fill in categories in [fillindb.py](https://github.com/Anubis-LT/PROJET10/blob/master/products/management/commands/fillindb.py)


## Built With

* [Django](https://www.djangoproject.com/)
* [Start Bootstrap Template](https://startbootstrap.com/themes/creative/)
* [OpenFoodFacts](https://fr.openfoodfacts.org/) : used for fill in database